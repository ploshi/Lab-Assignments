// Austin White
// file name: recursive.h
// 4/24/2015
// Basic class definition for reverse() and find().

#ifndef RECURSIVE_H
#define RECURSIVE_H

#include <iostream>
#include <string>

using namespace std;

class Sentence
{
public:
    Sentence (string aPhrase);
    string reverse();
    bool find(string sub);

private:
    string s;
};

#endif

