// Austin White
// file name: recursive.cpp
// 4/24/2015
// Functions reverse and find, in order to reverse a string recursively.  Also to find substrings within the string recursively.

#include <iostream>
#include <string>


#include "recursive.h"

using namespace std;


Sentence::Sentence(string aPhrase)
{
s=aPhrase;
}

string Sentence::reverse()
{

    if (s.length() == 1)
        {
            return s;
        }
    if (s.length() < 1)
        {
            return "";
        }
    
    string f = s.substr(0,1);
    string rest = s.substr(1, s.length()-1);
    Sentence s2(rest);
    string rs = s2.reverse()+ f;
    return rs;
        
}

bool Sentence::find(string sub)
{
    if(sub.size() > s.size())

    return false;

    if(sub == s.substr(0,sub.length()))

    return true;

    else

    s = s.substr(1,sub.length() - 1);
    
    find(sub);
}
