// Austin White
// file name: recursive_main.cpp
// 4/24/2015
// Main program to do unit testing on the reverse() and find() functions to ensure that they are working properly.

#include <iostream>
#include <string>

#include "recursive.h"

using namespace std;

main()

{
    Sentence first_sentence("Mississippi");
    cout << "Unit Test Number 1: " << endl;
    cout << first_sentence.reverse() << "\n";
    bool b = first_sentence.find("sip"); // returns true
    if( b == true)
    cout << "true" << endl;
    else
    cout << "false" << endl;

    Sentence second_sentence("Meme");
    cout << "Unit Test Number 2: " << endl;
    cout << second_sentence.reverse() << "\n";
    bool b2 = second_sentence.find("mem"); 
    if(b2==true)
    cout << "true" << endl;
    else
    cout << "false" << endl;

    Sentence third_sentence("Sunflower");
    cout << "Unit Test Number 3: " << "\n";
    cout << third_sentence.reverse() << "\n";
    bool b3 = third_sentence.find("wolf");
    if(b3==true)
    cout << "true" << endl;
    else
    cout << "false" << endl;

    
    Sentence fourth_sentence("Bingo");
    cout << "Unit Test Number 4: " << endl;
    cout << fourth_sentence.reverse() << "\n";
    bool b4 = fourth_sentence.find("watermelon");
    if(b4==true)
    cout << "true" << endl;
    else
    cout << "false" << endl;

 
    Sentence fifth_sentence("Question");
    cout << "Unit Test Number 5: " << endl;
    cout << fifth_sentence.reverse() << "\n";
    bool b5 = fifth_sentence.find("Correlation");
    if(b5==true)
    cout << "true" << endl;
    else
    cout << "false" << endl;

    return 0;
}
